# This file is a part of TG-FileStreamBot
# Coding : Jyothis Jayanth [@EverythingSuckz]

import sys
from os import getenv

from dotenv import load_dotenv


load_dotenv(override=True)
affirmative = ("1", "true", "yes", "y", "on")


class Var(object):
    MULTI_CLIENT = False
    MAINTENANCE = False
    ASYNC_TASKS = {}

    # important
    API_ID = int(getenv("API_ID"))
    API_HASH = str(getenv("API_HASH"))
    BOT_TOKEN = str(getenv("BOT_TOKEN") or getenv("LINKBOT_TOKEN"))
    BIN_CHANNEL = int(getenv("BIN_CHANNEL"))

    # custom
    USER_SESSION = getenv("USER_SESSION")
    USER_SLEEP_THRESHOLD = int(getenv("USER_SLEEP_THRESHOLD", "30"))
    REDIS_DATA = getenv("REDIS_DATA")
    TGLOGGER = getenv("TGLOGGER", "0").lower() in affirmative

    # optional
    SLEEP_THRESHOLD = int(getenv("SLEEP_THRESHOLD", "120"))  # 2 minute
    WORKERS = int(getenv("WORKERS", "6"))  # 6 workers = 6 commands at once
    PORT = int(getenv("PORT", "8080"))
    BIND_ADDRESS = str(getenv("WEB_SERVER_BIND_ADDRESS", "0.0.0.0"))
    PING_INTERVAL = int(getenv("PING_INTERVAL", "1200"))  # 20 minutes
    HAS_SSL = getenv("HAS_SSL", "0").lower() in affirmative
    NO_PORT = getenv("NO_PORT", "0").lower() in affirmative
    FQDN = str(getenv("FQDN", BIND_ADDRESS))
    URL = "http{}://{}{}/".format(
        "s" if HAS_SSL else "",
        FQDN,
        "" if NO_PORT else f":{PORT}",
    )

    # others
    KEEP_ALIVE = getenv("KEEP_ALIVE", "0").lower() in affirmative
    STATUS_UPDATE = getenv("STATUS_UPDATE", "0").lower() in affirmative
    DEBUG = getenv("DEBUG", "0").lower() in affirmative
    USE_SESSION_FILE = getenv("USE_SESSION_FILE", "0").lower() in affirmative
