# This file is a part of TG-FileStreamBot
# Coding : Jyothis Jayanth [@EverythingSuckz]

from . import tgbot, mention


@tgbot(pattern="start")
async def _start(e):
    await e.reply(
        f"Hi {mention(e.sender)}, Reply to any File with /link to get an Instant Stream Link."
    )
