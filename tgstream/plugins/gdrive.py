# Ultroid - UserBot
# Copyright (C) 2020-2023 TeamUltroid
#
# This file is a part of < https://github.com/TeamUltroid/Ultroid/ >
# PLease read the GNU Affero General Public License in
# <https://github.com/TeamUltroid/pyUltroid/blob/main/LICENSE>.

import asyncio
import base64
import json
import os
import time
from mimetypes import guess_type
from urllib.parse import parse_qs, urlencode

from aiohttp import ClientSession
from aiohttp.client_exceptions import ContentTypeError
from telethon.tl.types import Message
from telethon.errors import MessageNotModifiedError, MessageIdInvalidError

from tgstream.utils.FastTelethon import download_file
from . import logger, humanbytes, get_readable_time, DB, tgbot


class GDrive:
    __slots__ = (
        "base_url",
        "client_id",
        "client_secret",
        "creds",
        "folder_id",
        "scope",
    )

    def __init__(self):
        self.base_url = "https://www.googleapis.com/drive/v3"
        self.scope = "https://www.googleapis.com/auth/drive"

    @classmethod
    async def init(cls):
        self = cls()
        self.client_id = await DB._get_key("GDRIVE_CLIENT_ID")
        self.client_secret = await DB._get_key("GDRIVE_CLIENT_SECRET")
        self.folder_id = await DB._get_key("GDRIVE_FOLDER_ID") or "root"
        self.creds = await DB._get_key("GDRIVE_AUTH_TOKEN") or {}
        return self

    def get_oauth2_url(self):
        return "https://accounts.google.com/o/oauth2/v2/auth?" + urlencode(
            {
                "client_id": self.client_id,
                "redirect_uri": "http://localhost",
                "response_type": "code",
                "scope": self.scope,
                "access_type": "offline",
            }
        )

    async def get_access_token(self, code=None) -> dict:
        if code.startswith("http://localhost"):
            # get all url arguments
            code = parse_qs(code.split("?")[1]).get("code")[0]
        url = "https://oauth2.googleapis.com/token"
        async with ClientSession() as client:
            resp = await client.post(
                url,
                data={
                    "client_id": self.client_id,
                    "client_secret": self.client_secret,
                    "redirect_uri": "http://localhost",
                    "grant_type": "authorization_code",
                    "code": code,
                },
                headers={"Content-Type": "application/x-www-form-urlencoded"},
            )
            self.creds = await resp.json()
        self.creds["expires_in"] = time.time() + 3590
        await DB._set_key("GDRIVE_AUTH_TOKEN", self.creds)
        return self.creds

    async def refresh_access_token(self) -> None:
        async with ClientSession() as client:
            resp = await client.post(
                "https://oauth2.googleapis.com/token",
                data={
                    "client_id": self.client_id,
                    "client_secret": self.client_secret,
                    "grant_type": "refresh_token",
                    "refresh_token": self.creds.get("refresh_token"),
                },
                headers={"Content-Type": "application/x-www-form-urlencoded"},
            )
            self.creds["access_token"] = (await resp.json())["access_token"]
        self.creds["expires_in"] = time.time() + 3590
        await DB._set_key("GDRIVE_AUTH_TOKEN", self.creds)

    async def get_size_status(self) -> dict:
        if time.time() > self.creds.get("expires_in"):
            await self.refresh_access_token()
        async with ClientSession() as client:
            resp = await client.get(
                self.base_url + "about",
                headers={
                    "Authorization": "Bearer " + self.creds.get("access_token"),
                    "Content-Type": "application/json",
                },
                params={"fields": "storageQuota"},
            )
            return await resp.json()

    async def set_permissions(
        self, fileid: str, role: str = "reader", type: str = "anyone"
    ):
        # set permissions to anyone with link can view
        if time.time() > self.creds.get("expires_in"):
            await self.refresh_access_token()
        async with ClientSession() as client:
            resp = await client.post(
                self.base_url + f"/files/{fileid}/permissions",
                headers={
                    "Authorization": "Bearer " + self.creds.get("access_token"),
                    "Content-Type": "application/json",
                },
                json={
                    "role": role,
                    "type": type,
                },
            )
            return await resp.json()

    async def list_files(self) -> dict:
        if time.time() > self.creds.get("expires_in"):
            await self.refresh_access_token()
        async with ClientSession() as client:
            resp = await client.get(
                self.base_url + "/files",
                headers={
                    "Authorization": "Bearer " + self.creds.get("access_token"),
                    "Content-Type": "application/json",
                },
            )
            return await resp.json()

    async def delete(self, fileId: str) -> dict:
        if time.time() > self.creds.get("expires_in"):
            await self.refresh_access_token()
        async with ClientSession() as client:
            r = await client.delete(
                self.base_url + f"/files/{fileId}",
                headers={
                    "Authorization": "Bearer " + self.creds.get("access_token"),
                    "Content-Type": "application/json",
                },
            )
            try:
                return await r.json()
            except ContentTypeError:
                return {"status": "success"}

    async def copy_file(
        self, fileId: str, filename: str, folder_id: str, move: bool = False
    ):
        if time.time() > self.creds.get("expires_in"):
            await self.refresh_access_token()
        headers = {
            "Authorization": "Bearer " + self.creds.get("access_token"),
            "Content-Type": "application/json",
        }
        params = {
            "name": filename,
            "mimeType": "application/octet-stream",
            "fields": "id, name, webContentLink",
            "supportsAllDrives": "true",
        }
        file_metadata = {
            "name": filename,
            "fileId": fileId,
        }
        if folder_id:
            file_metadata["parents"] = [folder_id]
        elif self.folder_id:
            file_metadata["parents"] = [self.folder_id]
        params["addParents"] = folder_id if folder_id else self.folder_id
        params["removeParents"] = "root" if move else None
        async with ClientSession() as client:
            resp = await client.patch(
                self.base_url + f"/files/{fileId}",
                headers=headers,
                data=json.dumps(file_metadata),
                params=params,
            )
            r = await resp.json()
            if r.get("error") and r["error"]["code"] == 401:
                await self.refresh_access_token()
                return await self.copy_file(fileId, filename, folder_id, move)
            return r

    async def upload_file(
        self, event, path: str, filename: str = None, folder_id: str = None
    ):
        if time.time() > self.creds.get("expires_in"):
            await self.refresh_access_token()
        filename = filename if filename else path.split("/")[-1]
        mime_type = guess_type(path)[0] or "application/octet-stream"
        # upload with progress bar
        filesize = os.path.getsize(path)
        # await self.get_size_status()
        chunksize = 50 * 1024 * 1024  # default = 104857600  # 100MB
        # 1. Retrieve session for resumable upload.
        headers = {
            "Authorization": "Bearer " + self.creds.get("access_token"),
            "Content-Type": "application/json",
        }
        params = {
            "name": filename,
            "mimeType": mime_type,
            "fields": "id, name, webContentLink",
            "parents": [folder_id] if folder_id else [self.folder_id],
        }
        async with ClientSession() as client:
            r = await client.post(
                "https://www.googleapis.com/upload/drive/v3/files?uploadType=resumable",
                headers=headers,
                data=json.dumps(params),
                params={"fields": "id, name, webContentLink"},
            )
            if r.status == 401:
                await self.refresh_access_token()
                return await self.upload_file(event, path, filename, folder_id)
            elif r.status == 403:
                # upload to root and move
                r = await self.upload_file(event, path, filename, "root")
                return await self.copy_file(r["id"], filename, folder_id, move=True)
            upload_url = r.headers.get("Location")

        uploaded = 0
        trigger_count = 0
        resp = None
        last_txt = ""
        start = time.time()
        with open(path, mode="rb") as f:
            while filesize != uploaded:
                trigger_count += 1
                chunk_data = f.read(chunksize)
                headers = {
                    "Content-Length": str(len(chunk_data)),
                    "Content-Range": f"bytes {uploaded}-{uploaded + len(chunk_data) - 1}/{filesize}",
                }
                uploaded += len(chunk_data)
                async with ClientSession() as client:
                    resp = await client.put(
                        upload_url,
                        data=chunk_data,
                        headers=headers,
                    )
                diff = time.time() - start
                percentage = round((uploaded / filesize) * 100, 2)
                speed = round(uploaded / diff, 2)
                eta = round((filesize - uploaded) / speed, 2)
                crnt_txt = (
                    f"Uploading `{filename}` to **GDrive**...\n\n"
                    + f"**Status:**  `{humanbytes(uploaded)}/{humanbytes(filesize)}` » `{percentage}%`\n"
                    + f"**Speed:**  `{humanbytes(speed)}/s`\n"
                    + f"**ETA:**  `{get_readable_time(eta)}`"
                )
                if trigger_count % 4 == 0 and last_txt != crnt_txt:
                    await event.edit(crnt_txt)
                    last_txt = crnt_txt
            return await resp.json()


No_Flood = {}


async def progress(current, total, event, start, type_of_ps):
    jost = str(event.chat_id) + "_" + str(event.id)
    plog = No_Flood.get(jost)
    now = time.time()
    if plog and current != total:
        if (now - plog) < 8:  # delay = 8s
            return
    diff = now - start
    percentage = current * 100 / total
    speed = current / diff
    time_to_completion = round((total - current) / speed)
    bar_count = min(int(percentage // 5), 20)
    progress_str = "`[{0}{1}] {2}%`\n\n".format(
        "●" * bar_count,
        "" * (20 - bar_count),
        round(percentage, 2),
    )
    tmp = progress_str + "`{0} of {1}`\n\n`✦ Speed: {2}/s`\n\n`✦ ETA: {3}`\n\n".format(
        humanbytes(current),
        humanbytes(total),
        humanbytes(speed),
        get_readable_time(time_to_completion),
    )
    to_edit = "`✦ {}`\n\n{}".format(type_of_ps, tmp)
    try:
        No_Flood.update({jost: now})
        await event.edit(to_edit)
    except MessageNotModifiedError as exc:
        logger.warning("err in progress: message_not_modified")


async def tg_downloader(
    media,
    event,
    show_progress=False,
    **kwargs,
):
    if getattr(media.media, "document", None):
        filename = (
            getattr(media.file, "name", None) or str(time.time()) + media.file.ext
        )
        raw_file, edit_missed = None, 0
        start_time = time.time()
        while not raw_file:
            with open(filename, "wb") as f:
                try:
                    raw_file = await download_file(
                        client=event.client,
                        location=media.document,
                        out=f,
                        progress_callback=(
                            lambda d, t: asyncio.create_task(
                                progress(
                                    d,
                                    t,
                                    event,
                                    start_time,
                                    f"Downloading {filename} ...",
                                )
                            )
                        )
                        if show_progress
                        else None,
                    )
                except MessageNotModifiedError as exc:
                    edit_missed += 1
                    if edit_missed >= 6:
                        raise Exception(exc) from None
                except MessageIdInvalidError:
                    raise Exception(
                        f"Download Cancelled for '{filename}' because message was deleted."
                    ) from None

        return raw_file.name, time.time() - start_time

    # normal download.
    s_time = time.time()
    _callback = None
    if show_progress and not getattr(media, "photo", None):
        _callback = lambda d, t: asyncio.create_task(
            progress(d, t, event, s_time, "Downloading ...")
        )
    path = await event.client.download_media(
        media,
        progress_callback=_callback,
    )
    return path, time.time() - s_time


@tgbot(
    pattern="gup( (.*)|$)",
    admin_cmd=True,
)
async def gup(event):
    GD = await GDrive.init()
    if not GD.creds:
        return await event.reply(
            "`Credentials have not been added!... \n\nAdd GDRIVE_TOKEN to Use it..`"
        )
    input_file = event.pattern_match.group(2) or await event.get_reply_message()
    if not input_file:
        return await event.reply(
            "`Reply to file or give its location to upload to Gdrive!`"
        )

    mone = await event.reply("`Processing..`")
    if isinstance(input_file, Message):
        to_delete = True
        filename, t_time = await tg_downloader(media=input_file, event=mone)
        tt = get_readable_time(t_time)
        await mone.edit(f"Downloaded to \n`{filename}`\n in {tt}..")
        await asyncio.sleep(2)
    else:
        if not os.path.exists(input_file):
            return await mone.eor(
                "`File Not found in local server. Give me a file path :((`",
                time=6,
            )
        to_delete = False
        filename = input_file

    try:
        m_time = time.time()
        resp = await GD.upload_file(mone, filename)
        text = "**GDrive Upload was Successful!** \n\n**File Name:**  `{name}` \n**Size:**  `{size}` \n**GDrive Link:**  [Click Here]({link}) \n**Time Taken:**  `{time_taken}`"
        await asyncio.sleep(2)
        await mone.edit(
            text.format(
                name=resp.get("name"),
                size=humanbytes(os.path.getsize(filename)),
                link=resp.get("webContentLink"),
                time_taken=get_readable_time(time.time() - m_time),
            )
        )
        if to_delete:
            os.remove(filename)
    except Exception as exc:
        logger.exception(exc)
        await mone.edit(f"Exception occurred while uploading to gDrive: \n`{exc}`")
