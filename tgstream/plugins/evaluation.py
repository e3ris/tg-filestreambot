import asyncio
import os
import sys
import time
import traceback
from io import StringIO
from pathlib import Path
from shutil import which

from telethon.tl import types
from telethon.utils import get_display_name
from pyrogram.types import InlineKeyboardMarkup as ikm, InlineKeyboardButton as ikb

from ..clients import multi_clients, work_loads
from . import *


async def pyro_exec(code, client, message):
    exec(
        (
            "async def __pexec(client, message): "
            "\n  p = print"
            "\n  app = client"
            "\n  m = message"
            "\n  chat = m.chat.id"
            "\n  rm = reply = m.reply_to_message \n"
        )
        + "".join(("\n  " + l) for l in code.splitlines())
    )
    return await locals()["__pexec"](client, message)


async def tele_exec(code, event):
    exec(
        (
            "async def __texec(event, client): "
            "\n  p = print"
            "\n  bot = event.client"
            "\n  e = event"
            "\n  rm = reply = await event.get_reply_message()"
            "\n  chat = event.chat_id \n"
        )
        + "".join(("\n  " + l) for l in code.splitlines())
    )
    return await locals()["__texec"](event, event.client)


async def bash(cmd):
    process = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
        executable=which("bash"),
    )
    stdout, stderr = await process.communicate()
    err = stderr.decode(errors="replace").strip() or None
    out = stdout.decode(errors="replace").strip()
    return out, err


async def evalogger(cmd, e):
    msg = "<b>#DDL CMD Executed!</b> \n\n<code>{cmd}</code> \n\n–  {usr}:  <a href='{usr_mention}'>{usr_id}</a> \n–  <a href='{msg_link}'>{chat}</a>"
    sndr = e.sender
    chat = await DB.get_key("LINKBOT:DUMP")
    try:
        my_msg = msg.format(
            cmd=cmd,
            usr=get_display_name(sndr),
            usr_mention=f"tg://user?id={sndr.id}",
            usr_id=str(sndr.id),
            msg_link=e.message_link,
            chat=get_display_name(e.chat or await e.get_chat()),
        )
        await asyncio.sleep(2)
        await client.send_message(chat, my_msg, link_preview=False, parse_mode="html")
    except Exception:
        logger.exception("eval logger error")


@tgbot(pattern="p?eval", admin_cmd=True, args=True, take_edits=True)
async def multi_eval(e):
    try:
        cmd = e.text.split(" ", maxsplit=1)[1]
    except IndexError:
        return await e.reply("`Give some command as well..`")

    _client = "pyrogram" if "p" in e.text[:5] else "telethon"
    msg = await e.reply(f"`Processing...`")

    old_stderr = sys.stderr
    old_stdout = sys.stdout
    redirected_output = sys.stdout = StringIO()
    redirected_error = sys.stderr = StringIO()
    stdout, stderr, exc = None, None, None

    try:
        if _client == "pyrogram":
            message = await StreamBot.get_messages(e.chat_id, e.id)
            s_time = time.time()
            await pyro_exec(cmd, StreamBot, message)
        elif _client == "telethon":
            s_time = time.time()
            await tele_exec(cmd, e)
    except Exception:
        exc = traceback.format_exc()
    finally:
        f_time = time.time() - s_time
        del s_time

    stdout = redirected_output.getvalue()
    stderr = redirected_error.getvalue()
    f_time = get_readable_time(f_time) if f_time > 61 else f"{f_time:.3f}ms"
    sys.stdout = old_stdout
    sys.stderr = old_stderr
    evaluation = ""
    if exc:
        evaluation = exc
    elif stderr:
        evaluation = stderr
    elif stdout:
        evaluation = stdout
    else:
        evaluation = "Success"

    final_output = f"`#{_client}`  __({f_time})__ \n\n**>** ```{cmd}``` \n\n**>>** ```{evaluation.strip()}```"
    if len(final_output) > 4000:
        filename = f"{_client}-eval.txt"
        out = str(final_output).replace("`", "").replace("**", "").replace("__", "")
        with open(filename, "w+", encoding="utf8") as out_file:
            out_file.write(out)
        await e.reply(f"```{cmd[:1023]}```", file=filename)
        os.remove(filename)
        await msg.delete()
    else:
        await msg.edit(str(final_output))
    await evalogger(cmd, e)


@tgbot(pattern="(bash|exec|term)", admin_cmd=True, args=True)
async def shell_exec(e):
    try:
        cmd = e.text.split(" ", maxsplit=1)[1]
    except IndexError:
        return await e.reply("`Give some command as well..`")

    msg = await e.reply("`Processing ...`")

    out, err = await bash(cmd)
    xx = f"**~#** `{cmd}`\n\n"
    if not err and not out:
        xx += "**result ~#** \n`Success`"
    if err:
        xx += f"**error ~** \n```{err}``` \n\n"
    if out:
        _k = out.split("\n")
        o_ = "\n".join(_k)
        xx += f"**result ~** \n```{o_}```"
    if len(xx) > 4000:
        out = xx.replace("`", "").replace("**", "").replace("__", "")
        with open("bash.txt", "w+", encoding="utf8") as out_file:
            out_file.write(str(out))
        await e.reply(f"```{cmd[:1023]}```", file="bash.txt")
        await msg.delete()
        os.remove("bash.txt")
    else:
        await msg.edit(xx)
    await evalogger(cmd, e)
