import asyncio
from os import getpid
from subprocess import run
from pathlib import Path

from . import client, DB, tgbot, Var


@tgbot(pattern="logs", admin_cmd=True)
async def uplogs(e):
    msg = await e.reply("`Sending Logs..`")
    await asyncio.gather(
        e.reply("**Bot Logs**", file="botlogs.txt"),
        msg.delete(),
    )


@tgbot(pattern="restart( update)?", admin_cmd=True)
async def frestart(e):
    if e.pattern_match.group(1):
        msg = await e.reply("`Updating & Restarting...`")
        run("git reset --hard; git pull --rebase", shell=True)
    else:
        msg = await e.reply("`Restarting...`")
    Var.RESTART = True
    await asyncio.sleep(3)
    await client.disconnect()


@tgbot(pattern="shutdown", admin_cmd=True)
async def shut_down(e):
    msg = await e.reply("`Shutting Down..`")
    if Var.STATUS_UPDATE:
        await DB.del_key("LINKBOT:ACTIVE")
    await asyncio.sleep(3)
    await client.disconnect()
