# This file is a part of TG-FileStreamBot
# Coding : Jyothis Jayanth [@EverythingSuckz]

from urllib.parse import quote_plus

from telethon import events, Button
from telethon.errors import ButtonUrlInvalidError

from . import client, DB, mention, tgbot, Var


__types = ("video", "photo", "document", "sticker", "audio", "gif")

is_media = lambda e: (
    e.media
    and not getattr(e.media, "webpage", 0)
    and any(getattr(e, i, None) for i in __types)
)


async def generate_link(e):
    log_msg = await e.client.send_message(Var.BIN_CHANNEL, e.text, file=e.media)
    file_name = ("/" + quote_plus(e.file.name)) if e.file.name else ""
    main_url = await DB.get_key("LINKBOT:URL") or Var.URL
    pre_index = await DB.get_key("LINKBOT:INDEX") or "root"
    if not main_url.endswith("/"):
        main_url += "/"
    short_link = f"{main_url}{pre_index}/{Var.BIN_CHANNEL}/{log_msg.id}"
    stream_link = f"{short_link}{file_name}"
    text = f"<code>{stream_link}</code>\n\n~ (<a href='{short_link}'>shortened</a>)"
    try:
        buttons = [Button.url("Open 🖇️", url=stream_link)]
        await e.reply(text, buttons=buttons, parse_mode="html", link_preview=False)
    except ButtonUrlInvalidError:
        buttons = None
        await e.reply(text, parse_mode="html", link_preview=False)
    else:
        msg = f"**Generated link** \n\n#TGStreamer\nID - `{e.sender_id}`\nMention - {mention(e.sender)}"
        await log_msg.reply(msg, buttons=buttons)


@tgbot(pattern="link")
async def getlink(e):
    reply = await e.get_reply_message()
    if reply and is_media(reply):
        await generate_link(reply)
    else:
        await e.respond("`Reply to any TG file..`")
