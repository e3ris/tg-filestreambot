from time import perf_counter, time

from telethon.tl.functions import PingRequest

from tgstream import StartTime
from . import tgbot, get_readable_time


@tgbot(pattern="ping")
async def pingtg(e):
    msg = await e.reply("`Pong..`")
    start = perf_counter()
    await e.client(PingRequest(ping_id=0))
    end = perf_counter()
    m_s = round((end - start) * 1000, 3)
    uptime = get_readable_time(time() - StartTime)
    await msg.edit(f"**Pong !!** `{m_s} ms`\n**UpTime** - `{uptime}`")
