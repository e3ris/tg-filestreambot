# This file is a part of TG-FileStreamBot
# Coding : Jyothis Jayanth [@EverythingSuckz]

from os import makedirs

from pyrogram import Client
from telethon import TelegramClient

from .. import loop
from ..vars import Var
from ..version import __version__
from tgstream.utils.logger import logging as logger


lang_code = "fr"
device_model = "TG-Streamer"
workdir = "tgstream/clients/sessions"
makedirs(workdir, exist_ok=True)


client = TelegramClient(
    session=workdir + "/telethon_main",
    api_id=Var.API_ID,
    api_hash=Var.API_HASH,
    retry_delay=6,
    flood_sleep_threshold=Var.SLEEP_THRESHOLD,
    device_model=device_model,
    system_version=__version__,
    lang_code=lang_code,
    system_lang_code="en",
    loop=loop,
    base_logger="telethon",
)


StreamBot = Client(
    name="pyro_main",
    api_id=Var.API_ID,
    api_hash=Var.API_HASH,
    workdir=workdir,
    bot_token=Var.BOT_TOKEN,
    sleep_threshold=Var.SLEEP_THRESHOLD,
    # plugins={"root": "tgstream/plugins"},
    workers=Var.WORKERS,
    no_updates=True,
    device_model=device_model,
    system_version=__version__,
    lang_code=lang_code,
    message_cache=4096,
)


if Var.USER_SESSION:
    StreamUB = Client(
        name="pyro_ub",
        api_id=Var.API_ID,
        api_hash=Var.API_HASH,
        session_string=Var.USER_SESSION,
        no_updates=True,
        sleep_threshold=Var.USER_SLEEP_THRESHOLD,
        workers=Var.WORKERS,
        device_model=device_model,
        system_version=__version__,
        lang_code=lang_code,
        message_cache=2048,
    )
else:
    StreamUB = None
