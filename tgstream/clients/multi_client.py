# This file is a part of TG-FileStreamBot
# Coding : Jyothis Jayanth [@EverythingSuckz]

import asyncio
from os import environ

from pyrogram import Client
from pyrogram.raw.all import layer

from ..vars import Var
from ..version import __version__
from . import multi_clients, work_loads
from tgstream.utils.logger import logging as logger
from .main import device_model, lang_code, StreamBot, StreamUB, workdir
from .. import shutdown_tasks


async def start_client(client_id, token):
    try:
        logger.info(f"Starting Client {client_id}")
        client = Client(
            name=f"pyro_{client_id}",
            api_id=Var.API_ID,
            api_hash=Var.API_HASH,
            bot_token=token,
            workers=Var.WORKERS,
            sleep_threshold=Var.SLEEP_THRESHOLD,
            workdir=workdir,
            no_updates=True,
            device_model=device_model,
            system_version=__version__,
            lang_code=lang_code,
            message_cache=600,
        )
        await client.start()
        return client_id, client
    except Exception:
        logger.exception(f"Failed while starting client {client_id}")


async def start_mainbot():
    try:
        logger.info("Starting Client: 1")
        await StreamBot.start()
        return 1, StreamBot
    except Exception:
        logger.exception("Failed in starting pyro_main client")


async def start_user():
    try:
        logger.info(f"Starting User Client: (0)")
        await StreamUB.start()
        return 0, StreamUB
    except Exception:
        logger.exception("Failed in starting pyro_ub client")


async def initialize_clients():
    tasks = [start_mainbot()]
    all_clients = {
        int(var.rsplit("_", 1)[1]): token
        for var, token in environ.items()
        if var.startswith("MULTI_TOKEN_")
    }
    tasks.extend([start_client(i, token) for i, token in all_clients.items()])
    if StreamUB:
        tasks.append(start_user())

    logger.info(f"🔄 Initializing ({len(tasks)}) Pyrogram Clients..")
    output = await asyncio.gather(*tasks, return_exceptions=True)
    for val in output:
        if isinstance(val, Exception):
            logger.exception("Error in starting extra client..")
        elif val and type(val) == tuple:
            count, client = val
            multi_clients[count] = client
            work_loads[count] = 0

    logger.info(
        f"✨ Successfully Initialized {len(work_loads)} Pyro Clients on Layer {layer}!"
    )
    if len(multi_clients) == 0:
        logger.warning(
            "No active Pyrogram client was found, Web services will NOT work!"
        )
    elif len(multi_clients) == 1:
        logger.info("No additional Clients were found, Using the default Client.")
    else:
        Var.MULTI_CLIENT = True
        logger.info("Multi Client mode Enabled!")
