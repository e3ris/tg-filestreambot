# This file is a part of TG-FileStreamBot
# Coding : Jyothis Jayanth [@EverythingSuckz]

import asyncio
from importlib import import_module
from os import execl
from pathlib import Path
import sys

from aiohttp import web
from telethon.tl.alltlobjects import LAYER
from telethon.errors import FloodWaitError

from tgstream.clients import multi_clients
from tgstream.clients.main import client, StreamBot, StreamUB
from tgstream.clients.multi_client import initialize_clients
from tgstream.utils.keepalive import ping_server, update_status
from tgstream.utils.helpers import run_async_task
from tgstream.utils.stream_routes import _fix_cache, web_server
from tgstream.utils.database import DB
from tgstream import loop, Var, logger, shutdown_tasks


server = web.AppRunner(web_server())


def load_plugins():
    path = Path() / "tgstream" / "plugins"
    logger.info(f"Loading Plugins from {path}")
    allFiles = (
        i for i in path.glob("*.py") if i.is_file() and not i.name.startswith("__")
    )
    for file in allFiles:
        try:
            plug = str(file).replace(".py", "").replace("/", ".")
            import_module(plug)
            logger.info(f"Loaded Plugin - {file.name}")
        except BaseException as exc:
            logger.exception(f"Error in Loading Plugin - {file.name}")


async def start_services():
    # telethon startup
    logger.info(f"🔄 Initializing Telethon..")
    try:
        await client.start(bot_token=Var.BOT_TOKEN)
        logger.info(f"✨ Successfully Initialized Telethon Client on Layer {LAYER}!")
        client.me = await client.get_me()
    except FloodWaitError as exc:
        logger.exception(exc)
        quit(0)

    if Var.STATUS_UPDATE:
        run_async_task(update_status, id="status_update")
        shutdown_tasks.append(DB.del_key("LINKBOT:ACTIVE"))

    # multi client startup
    await initialize_clients()
    _fix_cache()

    # web server and keep alive setup
    logger.info(f"🔄 Initializing Web Setup..")
    await server.setup()
    await web.TCPSite(server, Var.BIND_ADDRESS, Var.PORT).start()

    # load plugins and startup
    load_plugins()
    logger.info(f"✨ Web Service Running at {Var.URL}")
    if Var.KEEP_ALIVE:
        run_async_task(ping_server, id="keep_alive")

    try:
        await client.send_message(
            Var.BIN_CHANNEL,
            f"#startup \nBot Serving at {Var.URL}",
            link_preview=False,
        )
    except Exception as exc:
        logger.exception(exc)


async def init_shutdown():
    try:
        await client.send_message(Var.BIN_CHANNEL, "#exiting")
    except Exception:
        pass
    sys.stdout.flush()
    await asyncio.sleep(5)
    shutdown_tasks.append(server.cleanup())
    for k, v in multi_clients.items():
        shutdown_tasks.append(v.stop())
    await asyncio.gather(*shutdown_tasks, return_exceptions=True)
    sys.stdout.flush()
    await asyncio.sleep(3)
    await asyncio.gather(
        loop.shutdown_asyncgens(),
        loop.shutdown_default_executor(),
        return_exceptions=True,
    )


def restartFunc():
    if not getattr(Var, "RESTART", None):
        sys.exit(0)
    python = sys.executable
    execl(python, python, "-m", "tgstream")


if __name__ == "__main__":
    try:
        loop.run_until_complete(start_services())
        loop.run_until_complete(client.disconnected)
    except BaseException as err:
        logger.exception(err)
    finally:
        logger.info("Stopping all Services..")
        loop.run_until_complete(init_shutdown())
        loop.stop()
        restartFunc()
