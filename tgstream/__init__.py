# This file is a part of TG-FileStreamBot
# Coding : Jyothis Jayanth [@EverythingSuckz]

import time

from tgstream.version import __version__


try:
    time.tzset()
except Exception:
    pass

StartTime = time.time()
shutdown_tasks = []


# init uvloop
import uvloop

uvloop.install()


# event loop
import asyncio

try:
    loop = asyncio.get_running_loop()
except RuntimeError:
    print("Initiating a new event loop..")
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)


# load Vars
from tgstream.vars import Var


# init logger
from tgstream.utils.logger import logging as logger


# init database
from tgstream.utils.database import init_db

loop.run_until_complete(init_db())


# import telepatch for its effects
try:
    import telethonpatch
except ImportError:
    logger.warning("Could not Import telethon patcher ..")
