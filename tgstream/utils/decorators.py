import asyncio
from functools import wraps
from re import compile

from telethon import Button
from telethon.events import MessageEdited, NewMessage, StopPropagation
from telethon.errors import FloodWaitError
from telethon.tl.types import MessageMediaWebPage

from .database import DB
from .logger import logging as logger
from tgstream.clients.main import client
from ..vars import Var


async def add_user_to_db(user_id):
    all_users = await DB.get_key("LINKBOT:USERS")
    if user_id not in all_users:
        all_users = list(all_users)
        all_users.append(user_id)
        await DB.set_key("LINKBOT:USERS", tuple(all_users))


def compile_pattern(pattern, take_args):
    if pattern:
        formet = rf"^[;/]{pattern}(?:@{client.me.username})?"
        formet += "" if take_args else "$"
        return compile(formet)


def tgbot(pattern=None, **kwargs):
    kwargs["forwards"] = False
    pm_cmd = kwargs.pop("pm_cmd", False)
    take_args = kwargs.pop("args", False)
    take_edits = kwargs.pop("take_edits", False)
    admin_cmd = kwargs.pop("admin_cmd", False)
    incoming_func = lambda e: not (
        (e.media and not isinstance(e.media, MessageMediaWebPage)) or e.via_bot_id
    )
    if "func" not in kwargs:
        kwargs["func"] = incoming_func

    def dec(func):
        @wraps(func)
        async def main(e):
            if pm_cmd and not e.is_private:
                return
            elif e.sender_id in await DB.get_key("LINKBOT:BANNED_USERS"):
                return
            elif e.media and not isinstance(e.media, MessageMediaWebPage):
                return

            await add_user_to_db(e.sender_id)

            try:
                await func(e)
            except FloodWaitError as fw:
                logger.error(f"Sleeping for {fw.seconds} seconds..")
                await asyncio.sleep(fw.seconds + 20)
            except StopPropagation:
                raise StopPropagation
            except KeyboardInterrupt:
                pass
            except Exception as exc:
                logger.exception(exc)

        nonlocal pattern
        pattern = compile_pattern(pattern, take_args)

        client.add_event_handler(
            main, NewMessage(incoming=True, pattern=pattern, **kwargs)
        )

        # edit for bot sudos only 🐸
        if take_edits:

            async def edit_func(e):
                if (
                    not incoming_func(e)
                    or (e.is_channel and e.chat.broadcast)
                    or getattr(e.message, "edit_hide", None)
                    or (e.message.edit_date - e.message.date).seconds > 1800
                ):
                    return
                sender = e.sender or await e.get_sender()
                return not e.via_bot_id and sender.id in await DB.get_key(
                    "LINKBOT:SUDO_USERS"
                )

            kwargs["func"] = edit_func
            client.add_event_handler(
                main, MessageEdited(incoming=True, pattern=pattern, **kwargs)
            )

        return main

    return dec
