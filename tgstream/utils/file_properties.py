import hashlib
from typing import Any, Optional, Union
from datetime import datetime

from pyrogram import Client
from pyrogram.types import Message
from pyrogram.file_id import FileId
from pyrogram.raw.types.messages import Messages

from .logger import logging as log
from .helpers import FileNotFoundError


def get_media_type(message: "Message") -> Any:
    media_types = (
        "audio",
        "document",
        "photo",
        "sticker",
        "animation",
        "video",
        "voice",
        "video_note",
    )
    for attr in media_types:
        media = getattr(message, attr, None)
        if media:
            return media


async def parse_file_unique_id(message: "Messages") -> Optional[str]:
    media = get_media_type(message)
    if media:
        return media.file_unique_id


async def get_file_ids(
    client: Client,
    chat_id: Union[str, int],
    message_id: int,
) -> Optional[FileId]:
    try:
        message = await client.get_messages(chat_id, message_id)
        if message.empty or not message.media:
            raise FileNotFoundError("Empty message or No Media in Message.")
    except Exception as exc:
        return log.error(exc)

    media = get_media_type(message)
    file_id = FileId.decode(media.file_id)
    setattr(file_id, "file_size", getattr(media, "file_size", 0))
    setattr(file_id, "mime_type", getattr(media, "mime_type", ""))
    setattr(file_id, "file_name", get_name(message))
    # file_unique_id = await parse_file_unique_id(message)
    # setattr(file_id, "unique_id", file_unique_id)
    return file_id


def get_hash(media_msg: Union[str, Message], length: int) -> str:
    if isinstance(media_msg, Message):
        media = get_media_type(media_msg)
        unique_id = getattr(media, "file_unique_id", "")
    else:
        unique_id = media_msg
    long_hash = hashlib.sha256(unique_id.encode("UTF-8")).hexdigest()
    return long_hash[:length]


def get_name(media_msg: Message) -> str:
    media = get_media_type(media_msg)
    file_name = getattr(media, "file_name", "") if media else ""
    if not file_name:
        media_type = media_msg.media.value if media_msg.media else "file"
        formats = {
            "photo": "jpg",
            "audio": "mp3",
            "voice": "ogg",
            "video": "mp4",
            "animation": "mp4",
            "video_note": "mp4",
            "sticker": "webp",
        }

        ext = formats.get(media_type)
        ext = "." + ext if ext else ""
        date = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        file_name = f"{media_type}-{date}{ext}"

    return file_name
