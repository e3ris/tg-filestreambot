import aiohttp
import asyncio

from ..vars import Var
from .database import DB
from .logger import logging as logger


async def ping_server():
    sleep_time = Var.PING_INTERVAL
    logger.info("Started Keep Alive with {}s interval.".format(sleep_time))
    while True:
        await asyncio.sleep(sleep_time)
        try:
            async with aiohttp.ClientSession(
                timeout=aiohttp.ClientTimeout(total=20)
            ) as session:
                async with session.get(Var.URL + "up") as resp:
                    if resp.status > 299:
                        logger.debug("ping_server response: {}".format(resp.status))
        except asyncio.TimeoutError:
            logger.warning("Couldn't connect to the site URL..")
        except Exception:
            logger.exception("Unexpected error: ")


async def update_status():
    while True:
        await DB.setex("LINKBOT:ACTIVE", 1210, 1)
        await asyncio.sleep(1200)
