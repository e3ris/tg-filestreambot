from ast import literal_eval
from asyncio import Lock
from copy import deepcopy
from redis.asyncio import Redis

from ..vars import Var
from .logger import logging as logger


alock = Lock()


class DataBase(Redis):
    def __init__(self, data):
        self._cache = {}
        password, host = data.split(maxsplit=1)
        if ":" in host:
            data = host.split(":")
            host = data[0]
            port = int(data[1])
        if host.startswith("http"):
            logger.critical("Your REDIS Var should not start with http..")
            quit(0)
        elif not (host and port):
            logger.critical("Port Number not found, Check your REDIS Variable.")
            quit(0)
        try:
            logger.info("Trying to Connect to Redis..")
            super().__init__(
                host=host,
                port=port,
                password=password,
                encoding="utf-8",
                decode_responses=True,
            )
        except Exception as e:
            logger.critical(f"Error while Connecting to Redis: {e}", exc_info=True)
            quit(0)

    async def _do_cache(self):
        if not await self.ping():
            logger.critical("Couldn't Ping the Redis DB")
            quit(0)
        for key in await self.keys():
            if key.startswith("LINKBOT:"):
                self._cache[key] = await self._get_key(key)
        logger.info(f"Cached {len(self._cache)} keys from Redis DB! 🌻✨")

    @staticmethod
    def run_eval(data):
        if not data:
            return data
        try:
            return literal_eval(data)
        except Exception:
            return data

    async def get_key(self, key):
        return deepcopy(self._cache.get(key))

    async def del_key(self, key):
        out = self._cache.pop(key, 0)
        return await self._del_key(key)

    async def set_key(self, key, value):
        self._cache[key] = self.run_eval(value)
        return await self._set_key(key, value)

    async def _get_key(self, key):
        async with alock:
            return self.run_eval(await self.get(key))

    async def _del_key(self, key):
        async with alock:
            return await self.delete(key)

    async def _set_key(self, key, value):
        async with alock:
            return await self.set(key, str(value))


DB = DataBase(Var.REDIS_DATA)


async def init_db():
    if Var.STATUS_UPDATE:
        activa = await DB._get_key("LINKBOT:ACTIVE")
        if activa:
            logger.critical("Found Key 'LINKBOT:ACTIVE' in DB..")
            logger.critical(f"Exiting for now, Fix DB :))")
            quit(0)

    await DB._do_cache()
    users = await DB.get_key("LINKBOT:USERS")
    if not users or type(users) != tuple:
        await DB.set_key("LINKBOT:USERS", tuple())
