# Taken from megadlbot_oss <https://github.com/eyaadh/megadlbot_oss/blob/master/mega/webserver/routes.py>
# Thanks to Eyaadh <https://github.com/eyaadh>

import asyncio
from math import ceil, floor
from mimetypes import guess_type
from os import getenv
from re import search
from secrets import token_hex
from time import time
from typing import Optional, Union

from aiohttp import web
from aiohttp.http_exceptions import BadStatusLine

from .. import StartTime
from .custom_dl import ByteStreamer
from .database import DB
from .helpers import FileNotFoundError, get_readable_time
from .logger import logging as logger
from ..clients.main import StreamUB
from ..clients import work_loads
from ..vars import Var
from ..version import __version__


alock = asyncio.Lock()
routes = web.RouteTableDef()

class_cache = {}
bot_work_loads = {}


def _fix_cache():
    from tgstream.clients import multi_clients

    global class_cache, bot_work_loads
    bot_work_loads = {k: 0 for k in multi_clients.keys() if k != 0}
    class_cache = {k: ByteStreamer(v) for k, v in multi_clients.items()}


def _get_int(s):
    try:
        return int(s)
    except ValueError:
        return s


def _get_status():
    return {
        "status": "running",
        "version": __version__,
        "uptime": get_readable_time(time() - StartTime),
        "total_bots": len(work_loads),
        "load": dict(
            (f"client{c}", l)
            for c, (_, l) in enumerate(
                sorted(work_loads.items(), key=lambda x: x[1], reverse=True),
                start=1,
            )
        ),
    }


def _handle_logs(r):
    data = r.get("True-Client-Ip")
    if not data.startswith(("3.129", "3.134")):
        try:
            datav6 = r.get("X-Forwarded-For").split(",", 1)[0]
        except Exception:
            datav6 = data
        msg = (
            r.get("Cf-Ipcountry")
            + " - "
            + data
            + (f" - {datav6}" if datav6 != data else "")
        )
        logger.debug(msg)


@routes.get(r"/{path:\S*}", allow_head=True)
async def stream_handler(request: web.Request):
    try:
        _handle_logs(request.headers)
        path = request.match_info["path"]

        if path == "":
            return web.json_response(_get_status())
        elif path == "up":
            return web.Response(text="Hello World!")
        elif Var.MAINTENANCE:
            return web.HTTPServiceUnavailable(
                text="Server Error 503: Server will Unavailable for some time, Please Try again Later."
            )

        pattern = r"^(\S+)\/([\w@-]+)\/(\d+)\/?(?:\S*)"
        match = search(pattern, path)
        if not match:
            return web.HTTPBadRequest(text="Error: could not find required IDS")

        first = match.group(1)
        streamkey = await DB.get_key("LINKBOT:INDEX") or "root"
        if first != str(streamkey):
            return web.HTTPServiceUnavailable(
                text="error: this is a private bot, plox go awey!"
            )

        index = None
        parsed = request.rel_url.query
        if parsed:
            if "dc" in parsed:
                try:
                    index = int(parsed.get("dc"))
                except ValueError:
                    return web.HTTPBadRequest(
                        text="Error: query 'dc' can not be non int char."
                    )
            if index == 0 or "ukey" in parsed:
                if _get_int(parsed.get("ukey")) == (
                    await DB.get_key("LINKBOT:USER_INDEX") or "u"
                ):
                    index = 0
                else:
                    index = None
            if index == 0 and not StreamUB:
                index = None
        chat_id = _get_int(match.group(2))
        message_id = int(match.group(3))
        return await media_streamer(request, chat_id, message_id, index)
    except FileNotFoundError as e:
        return web.HTTPNotFound(text=e.message)
    except (AttributeError, BadStatusLine, ConnectionResetError):
        pass
    except Exception as e:
        logger.exception(e)
        return web.HTTPInternalServerError(text=str(e))


async def media_streamer(
    request: web.Request,
    chat_id: Union[str, int],
    message_id: int,
    index: Optional[int],
):
    range_header = request.headers.get("Range", 0)

    # {0: user, (1,2,..): bots}
    if index == None:
        index = min(bot_work_loads, key=work_loads.get)
    tg_connect = class_cache[index]

    if index == 0:
        logger.info(f"user: fetching {chat_id}/{message_id}")
        async with alock:
            file_id = await tg_connect.get_file_properties(chat_id, message_id)
            await asyncio.sleep(0.6)
    else:
        logger.info(f"client {index}: fetching {chat_id}/{message_id}")
        file_id = await tg_connect.get_file_properties(chat_id, message_id)

    file_size = file_id.file_size

    if range_header:
        from_bytes, until_bytes = range_header.replace("bytes=", "").split("-")
        from_bytes = int(from_bytes)
        until_bytes = int(until_bytes) if until_bytes else file_size - 1
    else:
        from_bytes = request.http_range.start or 0
        until_bytes = (request.http_range.stop or file_size) - 1

    if (until_bytes > file_size) or (from_bytes < 0) or (until_bytes < from_bytes):
        return web.Response(
            status=416,
            body="416: Range not satisfiable",
            headers={"Content-Range": f"bytes */{file_size}"},
        )

    chunk_size = 1024 * 1024
    until_bytes = min(until_bytes, file_size - 1)

    offset = from_bytes - (from_bytes % chunk_size)
    first_part_cut = from_bytes - offset
    last_part_cut = until_bytes % chunk_size + 1

    req_length = until_bytes - from_bytes + 1
    part_count = ceil(until_bytes / chunk_size) - floor(offset / chunk_size)
    body = tg_connect.yield_file(
        file_id, index, offset, first_part_cut, last_part_cut, part_count, chunk_size
    )
    mime_type = file_id.mime_type
    file_name = file_id.file_name
    disposition = "attachment"

    if not mime_type:
        mime_type = guess_type(file_name)[0] or "application/octet-stream"

    if any((i in mime_type) for i in ("video/", "audio/", "/html")):
        disposition = "inline"

    return web.Response(
        status=206 if range_header else 200,
        body=body,
        headers={
            "Content-Type": f"{mime_type}",
            "Content-Range": f"bytes {from_bytes}-{until_bytes}/{file_size}",
            "Content-Length": str(req_length),
            "Content-Disposition": f'{disposition}; filename="{file_name}"',
            "Accept-Ranges": "bytes",
        },
    )


def web_server():
    web_app = web.Application(client_max_size=1024**2)
    web_app.add_routes(routes)
    return web_app
