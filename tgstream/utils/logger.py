import logging
from os import getenv
from platform import python_version

from ..vars import Var
from ..version import __version__


log_level = logging.DEBUG if Var.DEBUG else logging.INFO


file_handler = logging.FileHandler("botlogs.txt", mode="w", encoding="utf-8")
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(
    logging.Formatter("[%(levelname)s] - %(filename)s - %(message)s")
)
handlers = [file_handler, stream_handler]


if Var.TGLOGGER:
    from .tglogger import TGLogHandler

    token = getenv("MULTI_TOKEN_3", Var.BOT_TOKEN)
    tglogger = TGLogHandler(chat=Var.BIN_CHANNEL, token=token)
    tglogger.setLevel(logging.DEBUG)
    tglogger.setFormatter(
        logging.Formatter(
            "stream (%(levelname)s) (%(asctime)s)\n» %(filename)s - line %(lineno)s\n» %(message)s",
            datefmt="%d %b %H:%M:%S",
        )
    )
    handlers.append(tglogger)


logging.basicConfig(
    format="%(asctime)s - [%(levelname)s] - %(filename)s - %(message)s",
    datefmt="%d/%m/%y, %H:%M:%S",
    level=log_level,
    handlers=handlers,
)


class MyFilter(logging.Filter):
    def filter(self, log):
        ops = ("curl/7.88", "Python/3.12 aiohttp/3.9.")
        return not ("/up" in log.msg and any(i in log.msg for i in ops))


logging.getLogger("aiohttp.access").addFilter(MyFilter())
logging.getLogger("pyrogram").setLevel(logging.ERROR)
logging.getLogger("telethon").setLevel(logging.WARNING)
logging.getLogger("aiohttp").setLevel(logging.INFO)
logging.getLogger("aiohttp.web").setLevel(logging.INFO)

logging.info("Starting TG-Streamer Bot")
logging.info(f"Python version - {python_version()}")
logging.info(f"TG-Streamer Version - {__version__}")
