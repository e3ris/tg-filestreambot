from secrets import token_hex

from telethon.utils import get_display_name

from .. import loop
from ..vars import Var


def get_readable_time(seconds: int) -> str:
    count = 0
    readable_time = ""
    time_list = []
    time_suffix_list = ("s", "m", "h", "d")
    while count < 4:
        count += 1
        if count < 3:
            remainder, result = divmod(seconds, 60)
        else:
            remainder, result = divmod(seconds, 24)
        if seconds == 0 and remainder == 0:
            break
        time_list.append(int(result))
        seconds = int(remainder)
    for x in range(len(time_list)):
        time_list[x] = str(time_list[x]) + time_suffix_list[x]
    if len(time_list) == 4:
        readable_time += time_list.pop() + ", "
    time_list.reverse()
    readable_time += ": ".join(time_list)
    return readable_time


def mention(user):
    username = getattr(user, "username", None)
    if username:
        return "@" + username
    fullname = get_display_name(user)
    return f"[{fullname}](tg://user?id={user.id})"


def humanbytes(size):
    if not size:
        return "0 B"
    for unit in ("", "K", "M", "G", "T"):
        if size < 1024:
            break
        size /= 1024
    if type(size) == int:
        size = f"{size}{unit}B"
    elif type(size) == float:
        size = f"{size:.2f}{unit}B"
    return size


def run_async_task(func, *args, **kwargs):
    pid = kwargs.pop("id", None)
    while not pid or pid in Var.ASYNC_TASKS:
        pid = token_hex(nbytes=8)
    task = loop.create_task(func(*args, **kwargs))
    Var.ASYNC_TASKS[pid] = task
    task.add_done_callback(lambda task: Var.ASYNC_TASKS.pop(pid, 0))
    return pid


class InvalidHash(Exception):
    message = "Invalid hash"


class FileNotFoundError(Exception):
    message = "File not found"
